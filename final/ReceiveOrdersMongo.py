import pika, sys, os, json, pymongo
# receives WC order from a queue and print ID
#
# define callback fuction to extract ID from order and print, then add to MongDB
def callback(ch, method, properties, body):
        data=json.loads(body)
        for keyName, keyValue in data.items():
           if keyName == "id":
            orderId=keyValue
        print("Received Order", orderId)
#note IP address must be configured in Atlas
        try:
            mdbClient = pymongo.MongoClient(
                #"mongodb+srv://liamofthefunk:zWngWdtCXfWx8KS@cluster0.gepd9.azure.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
                "mongodb+srv://Woosage-User:8NrTPFsD2ePbbQC@cluster0.az051.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
            mdbDatabase = mdbClient.Woosage
            mdbCollection = mdbDatabase.Orders
            x = mdbCollection.insert_one(data)
            #print("Order Id: ", orderId, " inserted with inserted ID: ", x.inserted_id)
            print("Order Received", orderId)
            mdbClient.close()
        except pymongo.errors.PyMongoError as mdbError:
            print("MongoDB error encountered: ",mdbError)
#
def main():
# establish connection
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
    channel = connection.channel()
    # create queue
    channel.queue_declare(queue='wc_orders-conf', durable=True)
    # subscribe callback to queue and start consuming
    channel.basic_consume(queue='wc_orders-conf',
                        auto_ack=True,

                        on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
# wait for data and run callback when found
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

#woo webhook url
# https://wng.woosage.com/connector/connector.php

#mongo webhook url
#https: // webhooks.mongodb-realm.com/api/client/v2.0/app/order-app-tkinh/service/orders-app/incoming_webhook/webhook0

####
# TEST NEW CLUSTER SERVICE !!!

#8NrTPFsD2ePbbQC
