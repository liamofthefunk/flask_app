from flask.app import Flask
from flask import request
from flask.wrappers import Response
from logging import exception
import pika ,json, sys, os, pprint
from requests.models import codes

# app = Flask(__name__)

##
# DONE TODO INVOICE_LINES NEEDS TO BE INCLUDED IN THE SALES INVOICE, CURRENTLY ITS OUTSIDE.
# DONE TODO UNIT_PRICE AND DESCRIPTION ARE OUTSIDE INVOICE_LINES, NEED TO FIND A WAY TO INCLUDE THEM.
# DONE TODO QUANTITY MIGHT BE NEEDED, THE DOCS DONT SAY THAT IT IS BUT ERROR THROWN WHEN SENDING DATA WITHOUT IT.
# DONE TODO QUANTITY, TAX_RATE_ID, TAX_RATE NEED ADDING TO LINE_ITEMS
# DONE TODO ADDRESS NEEDS ADDING TO SALES_INVOICE
# DONE TODO LOOK AT QUANTITY

# TODO LOOK AT REFRESH TOKEN, WILL ALLOW US TO STAY CONNECTED FOR LONGER.
# TODO LOOK INTO HOW TO CREATE A SALES INVOICE FOR A NEW CLIENT, CURRENTLY WE ARE GETTING THE ID FROM AN EXISTING CUSTOMER


# @ CREATE CONTACT RECORD FOR NEW CUSTOMER? CHECK CUSTOMER IS NEW OR RETURNING? CONNECT CUSTOMER TO EXSISTING RECORD?
# ^ USED WEBSALES AFTER TALKING TO MICK, THIS WILL ALLOW US FOR NOW TO ASIGN ALL SALES TO ONE "CUSTOMER".
##

##################
sagedata = {
    "sales_invoice": {
        "contact_id": "6f8b8a66b94d437a9fb8ca809979c1d5",  # CONTACT_ID IS NOT THE USERS EMAIL BUT THE COMPANIES, WILL NEED A FEILD FOR CUSTOMER AND CLIENT
        "date": "null",
        "address" : "null",
        "reference" :"null",
        "invoice_lines": [
            {
                "description": "null",
                # "displayed_as": "E-Commerce Site (10000)"
                "ledger_account_id": "0d96638f180911e691e20a5d7cf84c3e",
                "unit_price": "null",
                "quantity" : "null"
                # TAX_RATE 
                
            }
        ]
        # "main_address": {
        #     "address_line_1": "null",
        #     "city": "null",
        #     "region": "null",
        #     "country_id": "null",
        #     "postal_code": "null"
        # }
    }
}

#####################

def callback(ch, method, properties, body):
    
    ### WORKING CODE ####
    data = json.loads(body)
    
    bill = data["billing"]
    ship = data["shipping"]

    strip = sagedata["sales_invoice"]
    liner = data["line_items"][0]["quantity"] # NEEDED TO DRILL DOWN WITH THIS, WAS GIVING AN ARRAY AS ANSWER 
    
    ####

    for keyName, keyValue in data.items():
        if keyName == "total":
            orderTotal = keyValue

    for keyName, keyValue in liner.items():
        if keyName == '$numberInt':
            orderVal = keyValue
            
    for keyName, keyValue in data.items():
        if keyName == "cart_hash":
            orderCart = keyValue
            
            sagedata['sales_invoice']['invoice_lines'] = [{
                "unit_price": orderTotal, 
                "description": orderCart,
                "tax_rate_id": "GB_STANDARD",
                "quantity" : orderVal, 
                "ledger_account_id": "0d95dabc180911e691e20a5d7cf84c3e"}]

    for keyName, keyValue in ship.items():
        if keyName == "postcode":
            orderAdd = {"address" : keyValue}
            strip.update(orderAdd)

#### WORKING CODE ####
    for keyName, keyValue in data.items():
        if keyName == 'date_created_gmt':
            orderDate = {"date": keyValue}
            strip.update(orderDate)

            print(json.dumps(sagedata))
            # https://github.com/Sage/sageone_api_java_sample/issues/22

### EMAIL WILL NEED LATER BUT FOR NOW NOT NEEDED            
    # for keyName, keyValue in bill.items():
    #     if keyName == "email":
    #         orderEmail = {"contact_id": keyValue}
    #         strip.update(orderEmail)
    #         pprint.pprint(sagedata)
#####

#CONNECT TO RABBIT
def main():
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
    channel = connection.channel()
    # create queue
    channel.queue_declare(queue='wc_orders-conf',
                            durable=True)
    # subscribe callback to queue and start consuming
    channel.basic_consume(queue='wc_orders-conf',
                        auto_ack=True,
                        on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()        


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
#COPY DATA

# @app.route('/', methods=['POST'])
# def webhook():
#     if request.headers['Content-Type'] == 'application/json':
#         data = request.json  
#         try:
# #SAGE CONNECTION
#             url = "https://api.accounting.sage.com/v3.1/contacts"
#             headers = {
#                 'Accept': 'application/json',
#                 'Authorization': 'eyJhbGciOiJSUzUxMiIsImtpZCI6IjRLR1RqZmN4TGpOWXdac1BiSjZkZ2Y1Zi1MdG9aSmp2cFR2c0hsUnFGX2s9In0.eyJqdGkiOiI0MTY2MmIzMC1hYzViLTRiYTgtOTM3Zi1iZGZjNzcxOTFjMTUiLCJpYXQiOjE2MzA1OTM3ODQsImV4cCI6MTYzMDU5NDA4NCwiaXNzIjoib2F1dGguYXdzLnNiYy1hY2NvdW50aW5nLnNhZ2UuY29tIiwic3ViIjoiYTMzMWZmZTgtNGJmMi05YjE2LTM5ZWYtZmQ1M2Q2ZmNmNWEwIiwiYXVkIjoiYXBpLnNiYy1hY2NvdW50aW5nLnNhZ2UuY29tIiwiYXpwIjoiNjkzNDdkMjctMWQ0OC00MWUzLWFmOGQtYWRjMjRjNmJjNzQ0L2NiNWM3ZWFjLWMwOTQtNDc3MS04MzBiLTE4MGNiNWEyYjY3MSIsImNvdW50cnkiOiJHQiIsInNjb3BlcyI6ImFjY291bnRpbmc6cncgY29yZTpydyIsInVzYWdlX3BsYW5fY29kZSI6ImV4dGVybmFsIn0.tjn9hlWDBtBAXF_P-10KmjN_reLaHexyDd1x3E05R9K4QvF327QCcTmuIJ_MZyH-jDIJobUhrPqo3h_SScvKe8Pkcv2qWalygZh-Tx-LYsB22Oz2U55IC6BymRVzXnXIWe7wUyjru1dLAk4bajw5iMQV8qHZx4xgjGlzX1kzlUEJcwL9LaFbYxkCyeuSkEkjzzuENI0gVY7rN4OoUkLvjLddXoeJPnHXnfmObvG8c6DamQq_6fG4C1ywu3vy7FyWMscRTXiavUv9Zd-67lKbOboCDsYMkZutLNxT0jvhE5qjOXWRaDMso1QwcDZR1bTv2L-co0ZdK_fQln6ckU2nAFyBssIfOD4suts3gd0iW9B7qrDLssw6iGt3dDLjA0cVaC9Axo-_1FnNig3Wlolb-1Uc1oZlP2ObowNn3SW4kzgsRsMLhWKspX3_EGC-JYQzTK_BLgZFsb2urOZbUd-RtG2tcuGbYAewD1A4J5N5yTIH_yozWlOOmFNBhjxmhvp0nM1amK-hGMHDVysVfF4Ra90DJ4p05y1MVXKITRBI3tr0o1Kugcvf2S-MU5RgYehA8MsIhl7IBlQfv1VfFg_VeGDgKiphhImmzo-M-7HEeDMjjLkxEmawEGHpMinlemr68nQgy9gNl1ZaOllW-m1WePbJWxaXdALwuDO_4_RhHbg'
#             }