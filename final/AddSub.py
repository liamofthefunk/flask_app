import pika
import sys
import os
import json

##
# https://www.geeksforgeeks.org/read-json-file-using-python/

f = open('subId.json')
dataId = json.load(f)


def callback(ch, method, properties, body):
    data = json.loads(body)
    for keyName, keyValue in data.items():
        # site
        if keyName == "":
            # siteURL
            url = keyValue

    sub = dataId[url]
    for SubName, SubValue in sub.items():
        if SubName == "woonum":
            subID = {"SubId": SubValue}
            data.update(subID)
            print(json.dumps(data))
##

# def callback(ch, method, properties, body):
#     data = json.loads(body)
#     for keyName, keyValue in data.items():
#            if keyName == "id":
#             orderId = keyValue
#     print("Received Order", orderId)


# dataId = json.load local file
# for name, value in data:
    # if name == website:
        # site name = value

        # if sitename == any site name in data2
            # append data['WooSage id']
# print (data)

# connection = pika.BlockingConnection(pika.URLParameters(
#     'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
# channel = connection.channel()
# channel.queue_declare(queue='stud')
# channel.basic_publish(exchange='topic.subsciber.id',
#                       routing_key='sub.1234',
#                       body=json.dumps(data))
# connection.close()


def main():
      # establish connection
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
    channel = connection.channel()
    # create queue
    channel.queue_declare(queue='wc_orders-conf', durable=True)
    # subscribe callback to queue and start consuming
    channel.basic_consume(queue='wc_orders-conf',
                          auto_ack=True,

                          on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
    
# wait for data and run callback when found
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
