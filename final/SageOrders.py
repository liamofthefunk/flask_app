import pika, sys, os, json
# receives WC order from a queue and print ID
#
# define callback fuction to extract ID from order and print, then add to MongDB
def callback(ch, method, properties, body):
        data=json.loads(body)
        for keyName, keyValue in data.items():
           if keyName == "id":
            orderId=keyValue
        print("Received Order", orderId)
#note IP address must be configured in Atlas
####SAGE API CONNECT

## https://developer.sage.com/api/accounting/api/invoicing-sales/#operation/postSalesEstimates
#API version
api_version = 3.1
#base url 
api_base_url = f"https://api.accounting.sage.com/{api_version}"
#endpoint
endpoint_path = f"/sales_invoices"
endpoint = f""
#ck

#sk

#REQUIRED FEILDS 
#contact_id // find out if that is the id of the customer or the client
#date = date of the invoice
#invoice_lines // array {}
#-description // description of invoice item
#-ledger_account_id // find out what the Ledger account is
#-unit_price // unit price for item



####
#
def main():
# establish connection
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
    channel = connection.channel()
    # create queue
    channel.queue_declare(queue='wc_orders-in')
    # subscribe callback to queue and start consuming
    channel.basic_consume(queue='wc_orders-in',
                        auto_ack=True,
                        on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
# wait for data and run callback when found
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)