import pika
import sys
import os

# TOPIC, QUEUE AND ROUTING KEY ALL NEED TO BE CHANGED FOR EACH CUSTOMER.


def main():
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))


    channel = connection.channel()
    
    # CHANGE QUEUE
    channel.queue_declare(queue='stud')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    # CHANGE QUEUE
    channel.basic_consume(
        queue='stud', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
