import pika, sys, os, json, pymongo
# receives WC order from a queue and print ID
#
# define callback fuction to extract ID from order and print, then add to MongDB
def callback(ch, method, properties, body):
        data=json.loads(body)
        for keyName, keyValue in data.items():
           if keyName == "WooSage Number":
            orderId=keyValue
        print("Received Order WooSage Number: ", orderId)
# note IP address must be configured in Atlas
#
def main():
# establish connection
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqps://ksljhpzg:b-GA2Sl_VIivj8zYUR0h9oFopdQRwR8Q@rattlesnake.rmq.cloudamqp.com/ksljhpzg'))
    channel = connection.channel()
    # create queue
    channel.queue_declare(queue='wc_orders-conf',
                            durable=True)
    # subscribe callback to queue and start consuming
    channel.basic_consume(queue='wc_orders-conf',
                        auto_ack=True,
                        on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
# wait for data and run callback when found
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

