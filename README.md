Server.py <br />
What does it do: <br />
Receives orders from WNG and then sends JSON to RabbitMQ.<br />
<br />
What needs to be done:<br />
As of right now I am using Ngrok to point WNG at, I will need to host server.py online so that we can point sites to it rather than just hosting it on my PC as it is now.

ReceveOrdersMongo.py<br />
What does it do:<br />
Takes order from RabbitMQ and sends it to MongoDB.<br />
<br />
What needs to be done:<br />
Nothing, everything seems to be working fine for I now.<br />
<br />
Test.py<br />
What does it do:<br />
Pulls order from RabbitMQ then strips out the required order information for sage and then sends the converted data to SageOne.<br />

What needs to be done:<br />
As of right now the data is being pulled and converted into the correct format and fields but sage has thrown up issues when trying to add the data as a new order. I am still working out what needs to be done. <br />
<br />
<br />
![Screenshot](Screenshot.png)
<br />
<br />
How to use:<br />
•	Download and run Ngrok creating a temporary address.<br />
•	To run Ngrok navigate to the folder with the app and open you terminal from that location, with the terminal open use the command “.\ngrok.exe http 5000”<br />
•	Copy the forwarding address starting http://<br />
•	Navigate to the webhooks section of the WooCommerce site that you are connecting to.<br />
•	Create a webhook and use the Ngrok address as the delivery URL, save your webhook.<br />
<br />
•	Run server.py.<br />
•	Run receveordersmongo.py<br />
•	Run test.py<br />
<br />
•	Now make an order on your WooCommerce site. <br />
